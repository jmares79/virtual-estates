<?php

namespace VirtualEstates\UserBundle\Service;

use VirtualEstates\UserBundle\Entity\User as UserEntity;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Doctrine\ORM\EntityManager;

class User
{
    private $em;
    private $validator;
    private $serializer;
    private $contactService;
    const DATE_FORMAT = 'd-m-Y';

    public function __construct(
        EntityManager $em, 
        RecursiveValidator $validator, 
        \JMS\Serializer\Serializer $serializer,
        \VirtualEstates\API\ContactBundle\Service\Contact $contactService
        )
    {
        $this->em = $em;
        $this->validator = $validator;
        $this->serializer = $serializer;
        $this->contactService = $contactService;
    }

    public function createUser($data)
    {
        $user = $this->setUser($data);
        $errors = $this->validator->validate($user);

        if (count($errors) == 0) {
            $this->em->persist($user);
            $this->em->flush();
            
            return $user;
        } else {
            throw new \Symfony\Component\Validator\Exception\ValidatorException($errors);
        }
     }

    protected function setUser($data)
    {
        $user = new UserEntity();

        $user->setName($data['name']);
        $user->setLastname($data['lastname']);
        $user->setUsername($data['username']);
        $user->setPassword($data['password']);
        $user->setEmail($data['email']);
        $user->setType($data['type']);
        $user->setBirth(\DateTime::createFromFormat(self::DATE_FORMAT, str_replace('/', '-', $data['birth'])));
        $user->setCreated(new \Datetime());
        $user->setEnabled(true);

        return $user;
    }

    public function deleteUsers()
    {
       $this->em->createQuery('DELETE FROM UserBundle:User')->execute();
    }

    public function updateUser($user, $data)
    {
        $user = $this->updateFields($user, $data);
        $errors = $this->validator->validate($user);

        if (count($errors) == 0) {
            $this->em->persist($user);
            $this->em->flush();
            
            return $user;
        } else {
            throw new \Exception($errors);
        }
     }

    public function hasValidDataForUpdate($data)
    {
        if (isset($data['username']) && !empty($data['username'])) {
            return true;
        } else {
            return false;
        }
    }

    protected function updateFields($user, $data)
    {
        foreach ($data as $field => $value) {
            $method = 'set'.ucfirst($field);

            if (!empty($value)) {
                $user->$method($value);
            }
        }

        if (isset($data['birth']) && !empty($data['birth'])) {
            $user->setBirth(\DateTime::createFromFormat(self::DATE_FORMAT, str_replace('/', '-', $data['birth'])));
        }

        $user->setUpdated(new \Datetime());

        return $user;
    }

    public function getContactsByUser($user)
    {
        return $this->serializer->serialize($user->getContacts(), 'json');
    }

    public function getSerializedUsers()
    {
        $users = $this->em->getRepository('UserBundle:User')->findAll();

        return $this->serializer->serialize($users, 'json');
    }

    public function addContact($data, $username)
    {
        $user = $this->em->getRepository('UserBundle:User')->findOneByUsername($username);

        if (null == $user) throw new HttpException(Codes::HTTP_NOT_FOUND);
        
        $contact = $this->contactService->createContact($data);
        $contact->setUser($user);
        $user->addContact($contact);
        
        $this->em->persist($user);
        $this->em->persist($contact);
        $this->em->flush();

        return $contact;
    }
}