<?php 

namespace VirtualEstates\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VirtualEstates\UserBundle\Entity\User;
use VirtualEstates\CommonBundle\Util\Util;

class Users implements FixtureInterface
{
    public function getOrder()
    {
        return 10;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $user = new User();

            $user->setName($this->getName());
            $user->setLastname($this->getLastname());
            $user->setUsername('user'.$i);
            $user->setPassword('user'.$i);
            $user->setEmail($user->getUsername().'@'.$this->getEmailHost());
            $user->setEnabled($this->getEnabled());
            $user->setType($this->getType());
            $user->setBirth(new \DateTime('now - '.rand(1000, 1500).' days'));

            $manager->persist($user);
        }

        $manager->flush();
    }

    protected function getName()
    {
        $male = array(
            'Antonio', 'José', 'Manuel', 'Francisco', 'Juan', 'David',
            'José Antonio', 'José Luis', 'Jesús', 'Javier', 'Francisco Javier',
            'Carlos', 'Daniel', 'Miguel', 'Rafael', 'Pedro', 'José Manuel',
            'Ángel', 'Alejandro', 'Miguel Ángel', 'José María', 'Fernando',
            'Luis', 'Sergio', 'Pablo', 'Jorge', 'Alberto'
        );

        $female = array(
            'María Carmen', 'María', 'Carmen', 'Josefa', 'Isabel', 'Ana María',
            'María Dolores', 'María Pilar', 'María Teresa', 'Ana', 'Francisca',
            'Laura', 'Antonia', 'Dolores', 'María Angeles', 'Cristina', 'Marta',
            'María José', 'María Isabel', 'Pilar', 'María Luisa', 'Concepción',
            'Lucía', 'Mercedes', 'Manuela', 'Elena', 'Rosa María'
        );

        if (rand() % 2) {
            return $male[array_rand($male)];
        } else {
            return $female[array_rand($female)];
        }
    }

    protected function getLastname()
    {
        $lastnames = array(
            'García', 'González', 'Rodríguez', 'Fernández', 'López', 'Martínez',
            'Sánchez', 'Pérez', 'Gómez', 'Martín', 'Jiménez', 'Ruiz',
            'Hernández', 'Díaz', 'Moreno', 'Álvarez', 'Muñoz', 'Romero',
            'Alonso', 'Gutiérrez', 'Navarro', 'Torres', 'Domínguez', 'Vázquez',
            'Ramos', 'Gil', 'Ramírez', 'Serrano', 'Blanco', 'Suárez', 'Molina',
            'Morales', 'Ortega', 'Delgado', 'Castro', 'Ortíz', 'Rubio', 'Marín',
            'Sanz', 'Iglesias', 'Nuñez', 'Medina', 'Garrido'
        );

        return $lastnames[array_rand($lastnames)].' '.$lastnames[array_rand($lastnames)];
    }

    protected function getEmailHost()
    {
        $mail = array('gmail.com', 'hotmail.com', 'yahoo.com');

        return $mail[array_rand($mail)];
    }

    protected function getType()
    {
        $types = array('Administrator', 'Commercial', 'Salesperson', 'IT');

        return $types[array_rand($types)];
    }

    protected function getEnabled()
    {
        return rand() % 2 ? true : false;
    }
}
