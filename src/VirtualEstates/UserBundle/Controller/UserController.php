<?php

namespace VirtualEstates\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\View\View;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use VirtualEstates\UserBundle\Entity\User;

class UserController extends Controller
{
    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns all users",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     * @Route("/", name="users_default")
     * @Route("/users", name="get_all_users")
     * @Method("GET")
     *
     * @return array
     */
    public function getUsersAction()
    {
        $service = $this->get('user.service');
  
        return new Response($service->getSerializedUsers(), Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns a single user",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     *
     * @Route("/user/{username}", name="get_user", requirements={"username"=".+"})
     * @Method("GET")
     *
     * @param string $username
     * @return array
     */
    public function getUserAction($username)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->findOneByUsername($username);

        if (null == $user) {
            throw new HttpException(Codes::HTTP_NOT_FOUND, "The user was not found");
        }

        return array('user' => $user);
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a user",
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Returned when there was an error on creation"
     *   }
     * )
     *
     * @Route("/user/{username}", name="post_user")
     * @Method("POST")
     *
     * @Annotations\View()
     *
     * @param mixin Data in the POST request
     */
    public function postUserAction(Request $request)
    {
        $data = $request->request->all();
        
        if (null == $data) {
            return View::create($data, Codes::HTTP_BAD_REQUEST);
        }

        $service = $this->get('user.service');

        try {
            $user = $service->createUser($data);

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setStatusCode(Codes::HTTP_CREATED);

            $response->headers->set('Location',
                $this->generateUrl('get_user', array('username' => $user->getUsername()), true)
            );

            return $response;
        } catch (Exception $e) {
            return View::create($data, Codes::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Updates a user",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     201 = "Returned when created",
     *     204 = "Returned when no content received",
     *     400 = "Returned when there was an error on creation"
     *   }
     * )
     *
     * @Route("/user", name="put_user")
     * @Method("PUT")
     *
     * @Annotations\View()
     *
     * @param mixin Data in the PUT request
     */
    public function putUserAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $service = $this->get('user.service');
        $data = $request->request->all();

        if (null == $data) {
            return View::create($data, Codes::HTTP_NO_CONTENT);
        } 

        if (!$service->hasValidDataForUpdate($data)) {
            return View::create($data, Codes::HTTP_BAD_REQUEST);
        }

        $user = $em->getRepository('UserBundle:User')->findOneByUsername($data['username']);

        if (null == $user) {
            return View::create($data, Codes::HTTP_NOT_FOUND);
        }

        try {
            $service->updateUser($user, $data);

            return View::create($data, Codes::HTTP_OK);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return View::create($data, Codes::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns all contacts by a user",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     * @Route("contacts/user/{username}", name="get_user_contacts", requirements={"username"=".+"}) 
     * @Method("GET")
     *
     * @return Response
     */
    public function getUserContactsAction($username)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->findOneByUsername($username);

        if (null == $user) {
            throw new HttpException(Codes::HTTP_NOT_FOUND, "The user was not found");
        }

        $service = $this->get('user.service');

        return new Response($service->getContactsByUser($user), Codes::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Adds a contact to a user",
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Returned when there was an error on creation"
     *   }
     * )
     *
     * @Route("/user/contact/{username}", name="post_user_contact", requirements={"username"=".+"}) 
     * @Method("POST")
     *
     * @param Request $request
     * @param string $username The username of the required user
     *
     * @return Response
     */
    public function postUserContactAction(Request $request, $username)
    {
        $data = $request->request->all();
        $service = $this->get('user.service');

        if (null == $data) {
            return new Response($data, Codes::HTTP_NO_CONTENT);
        }

        try {
            $contact = $service->addContact($data, $username);

            $response = new Response();
            $response->headers->set('Location',
                $this->generateUrl('get_contact', array('contactId' => $contact->getContactId()), true)
            );

            return $response;
        } catch (HttpException $e) {
            return new Response($data, Codes::HTTP_NOT_FOUND);
        } catch (\Symfony\Component\Validator\Exception\ValidatorException $e) {
            return new Response(json_encode($e->getMessage()), Codes::HTTP_BAD_REQUEST);
        }
    }
}
