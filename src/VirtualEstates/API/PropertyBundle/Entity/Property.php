<?php

namespace VirtualEstates\API\PropertyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use VirtualEstates\API\ContactBundle\Entity\Contact;

/**
 * Property
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Property
{
    /**
     * @var integer
     *
     * @ORM\Column(name="property_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $propertyId;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="VirtualEstates\API\ContactBundle\Entity\Contact", inversedBy="properties")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="contact_id")
     */
    private $contact;

    /**
     * @ORM\ManyToMany(targetEntity="PropertyType")
     * @ORM\JoinTable(
     *      name="property_by_type",
     *      joinColumns={@ORM\JoinColumn(name="property_id", referencedColumnName="property_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="property_type_id", referencedColumnName="property_type_id")}
     * )
     */
    private $propertyTypes;

    /**
     * @var integer
     *
     * @ORM\Column(name="bathrooms", type="integer")
     * @Assert\NotBlank()
     */
    private $bathrooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="parkings", type="integer", nullable=true)
     */
    private $parkings;

    /**
     * @var integer
     *
     * @ORM\Column(name="bedrooms", type="integer")
     * @Assert\NotBlank()
     */
    private $bedrooms;

    /**
     * @var float
     *
     * @ORM\Column(name="total_surface", type="float")
     * @Assert\NotBlank()
     */
    private $totalSurface;

    /**
     * @var float
     *
     * @ORM\Column(name="covered_surface", type="float", nullable=true)
     */
    private $coveredSurface;

    /**
     * @var float
     *
     * @ORM\Column(name="uncovered_surface", type="float", nullable=true)
     */
    private $uncoveredSurface;

    /**
     * @var float
     *
     * @ORM\Column(name="field_surface", type="float", nullable=true)
     */
    private $fieldSurface;

    /**
     * @var float
     *
     * @ORM\Column(name="field_width", type="float", nullable=true)
     */
    private $fieldWidth;

    /**
     * @var float
     *
     * @ORM\Column(name="field_ground", type="float", nullable=true)
     */
    private $fieldGround;

    /**
     * @var string
     *
     * @ORM\Column(name="construction_year", type="string", length=8, nullable=true)
     */
    private $constructionYear;

    /**
     * @var integer
     *
     * @ORM\Column(name="floor", type="integer")
     * @Assert\NotBlank()
     */
    private $floor;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_floors", type="integer")
     * @Assert\NotBlank()
     */
    private $totalFloors;

    /**
     * @var float
     *
     * @ORM\Column(name="monthly_maintenance", type="float")
     * @Assert\NotBlank()
     */
    private $monthlyMaintenance;

    /**
     * @ORM\ManyToMany(targetEntity="Amenity")
     * @ORM\JoinTable(name="properties_amenities",
     *      joinColumns={@ORM\JoinColumn(name="property_id", referencedColumnName="property_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="amenity_id", referencedColumnName="amenity_id")}
     * )
     */
    private $amenities;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

    public function __construct()
    {
        $this->propertyTypes = new ArrayCollection();
        $this->amenities = new ArrayCollection();
    }

    /**
     * Get property_id
     *
     * @return integer
     */
    public function getPropertyId()
    {
        return $this->propertyId;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Property
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set user
     *
     * @param string $user
     *
     * @return Property
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set contact
     *
     * @param string $contact
     *
     * @return Property
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set bathrooms
     *
     * @param integer $bathrooms
     *
     * @return Property
     */
    public function setBathrooms($bathrooms)
    {
        $this->bathrooms = $bathrooms;

        return $this;
    }

    /**
     * Get bathrooms
     *
     * @return integer
     */
    public function getBathrooms()
    {
        return $this->bathrooms;
    }

    /**
     * Set parkings
     *
     * @param integer $parkings
     *
     * @return Property
     */
    public function setParkings($parkings)
    {
        $this->parkings = $parkings;

        return $this;
    }

    /**
     * Get parkings
     *
     * @return integer
     */
    public function getParkings()
    {
        return $this->parkings;
    }

    /**
     * Set bedrooms
     *
     * @param integer $bedrooms
     *
     * @return Property
     */
    public function setBedrooms($bedrooms)
    {
        $this->bedrooms = $bedrooms;

        return $this;
    }

    /**
     * Get bedrooms
     *
     * @return integer
     */
    public function getBedrooms()
    {
        return $this->bedrooms;
    }

    /**
     * Set totalSurface
     *
     * @param float $totalSurface
     *
     * @return Property
     */
    public function setTotalSurface($totalSurface)
    {
        $this->totalSurface = $totalSurface;

        return $this;
    }

    /**
     * Get totalSurface
     *
     * @return float
     */
    public function getTotalSurface()
    {
        return $this->totalSurface;
    }

    /**
     * Set coveredSurface
     *
     * @param float $coveredSurface
     *
     * @return Property
     */
    public function setCoveredSurface($coveredSurface)
    {
        $this->coveredSurface = $coveredSurface;

        return $this;
    }

    /**
     * Get coveredSurface
     *
     * @return float
     */
    public function getCoveredSurface()
    {
        return $this->coveredSurface;
    }

    /**
     * Set uncoveredSurface
     *
     * @param float $uncoveredSurface
     *
     * @return Property
     */
    public function setUncoveredSurface($uncoveredSurface)
    {
        $this->uncoveredSurface = $uncoveredSurface;

        return $this;
    }

    /**
     * Get uncoveredSurface
     *
     * @return float
     */
    public function getUncoveredSurface()
    {
        return $this->uncoveredSurface;
    }

    /**
     * Set fieldSurface
     *
     * @param float $fieldSurface
     *
     * @return Property
     */
    public function setFieldSurface($fieldSurface)
    {
        $this->fieldSurface = $fieldSurface;

        return $this;
    }

    /**
     * Get fieldSurface
     *
     * @return float
     */
    public function getFieldSurface()
    {
        return $this->fieldSurface;
    }

    /**
     * Set fieldWidth
     *
     * @param float $fieldWidth
     *
     * @return Property
     */
    public function setFieldWidth($fieldWidth)
    {
        $this->fieldWidth = $fieldWidth;

        return $this;
    }

    /**
     * Get fieldWidth
     *
     * @return float
     */
    public function getFieldWidth()
    {
        return $this->fieldWidth;
    }

    /**
     * Set fieldGround
     *
     * @param float $fieldGround
     *
     * @return Property
     */
    public function setFieldGround($fieldGround)
    {
        $this->fieldGround = $fieldGround;

        return $this;
    }

    /**
     * Get fieldGround
     *
     * @return float
     */
    public function getFieldGround()
    {
        return $this->fieldGround;
    }

    /**
     * Set constructionYear
     *
     * @param string $constructionYear
     *
     * @return Property
     */
    public function setConstructionYear($constructionYear)
    {
        $this->constructionYear = $constructionYear;

        return $this;
    }

    /**
     * Get constructionYear
     *
     * @return string
     */
    public function getConstructionYear()
    {
        return $this->constructionYear;
    }

    /**
     * Set floor
     *
     * @param integer $floor
     *
     * @return Property
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return integer
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set totalFloors
     *
     * @param integer $totalFloors
     *
     * @return Property
     */
    public function setTotalFloors($totalFloors)
    {
        $this->totalFloors = $totalFloors;

        return $this;
    }

    /**
     * Get totalFloors
     *
     * @return integer
     */
    public function getTotalFloors()
    {
        return $this->totalFloors;
    }

    /**
     * Set monthlyMaintenance
     *
     * @param float $monthlyMaintenance
     *
     * @return Property
     */
    public function setMonthlyMaintenance($monthlyMaintenance)
    {
        $this->monthlyMaintenance = $monthlyMaintenance;

        return $this;
    }

    /**
     * Get monthlyMaintenance
     *
     * @return float
     */
    public function getMonthlyMaintenance()
    {
        return $this->monthlyMaintenance;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Property
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Property
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set amenities
     *
     * @param string $amenities
     *
     * @return Property
     */
    public function setAmenities($amenities)
    {
        $this->amenities = $amenities;

        return $this;
    }

    /**
     * Get amenities
     *
     * @return string
     */
    public function getAmenities()
    {
        return $this->amenities;
    }
}

