<?php

namespace VirtualEstates\API\PropertyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PropertyType
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PropertyType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="property_type_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $propertyTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * Get propertyTypeId
     *
     * @return integer
     */
    public function getPropertyTypeId()
    {
        return $this->propertyTypeId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PropertyType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PropertyType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}

