<?php

namespace VirtualEstates\API\PropertyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Amenity
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Amenity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="amenity_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $amenityId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;


    /**
     * Get amenityId
     *
     * @return integer
     */
    public function getAmenityId()
    {
        return $this->amenityId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Amenity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Amenity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}

