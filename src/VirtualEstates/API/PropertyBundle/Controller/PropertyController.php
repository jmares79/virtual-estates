<?php

namespace VirtualEstates\API\PropertyBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Util\Codes;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Validator\Exception\ValidatorException;

class PropertyController extends Controller
{
    /**
     * @ApiDoc(
     *   description = "Returns all properties",
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Route("/properties", name="get_all_properties")
     * @Method("GET")
     *
     * @return Response
     */
    public function getPropertiesAction()
    {
        $service = $this->get('virtual_estates_api_property');

        return new Response($service->getSerializedProperties(), Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *   description = "Returns all properties",
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Route("/property/{propertyId}", name="get_property", requirements={"propertyId"="\d+"})
     * @Method("GET")
     *
     * @param int $propertyId
     *
     * @return Response
     */
    public function getPropertyAction($propertyId)
    {
        $em = $this->getDoctrine()->getManager();
        $property = $em->getRepository('VirtualEstatesAPIPropertyBundle:Property')->findOneByPropertyId($propertyId);

        if (null == $property) return new Response('Resource not found', Response::HTTP_NOT_FOUND);

        $serializer = $this->get('jms_serializer');

        return new Response($serializer->serialize($property, 'json'), Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *   description = "Returns all properties for a given contact",
     *   statusCodes = {
     *      200 = "Returned when successful",
     *      404 = "Returned when resource not found"
     *   }
     * )
     *
     * @Route("/properties/{contactId}", name="get_property_by_contact", requirements={"contactId"="\d+"})
     * @Method("GET")
     *
     * @param int $contactId
     *
     * @return Response
     */
    public function getContactPropertiesAction($contactId)
    {
        $em = $this->getDoctrine()->getManager();
        $contact = $em->getRepository('VirtualEstatesAPIContactBundle:Contact')->findOneByContactId($contactId);

        if (null == $contact) return new Response('Resource not found', Response::HTTP_NOT_FOUND);

        $service = $this->get('virtual_estates_api_property');

        return new Response($service->getSerializedPropertiesByContact($contact), Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Adds a property to a contact",
     *   statusCodes = {
     *     201 = "Returned when property was successfully created",
     *     400 = "Returned when there was an error on request",
     *     422 = "Returned when there is invalid entity data"
     *   }
     * )
     *
     * @Route("/properties/contact/{contactId}", name="post_property_by_contact", requirements={"contactId"="\d+"})
     * @Method("POST")
     *
     * @param Request $request
     * @param int $contactId The id of the contact to whom the property belongs
     *
     * @return Response
     */
    public function postPropertyContactAction(Request $request, $contactId)
    {
        $data = $request->request->all();
        $serializer = $this->get('jms_serializer');

        if (null == $data) {
            return new Response($serializer->serialize($data, 'json'), Codes::HTTP_NO_CONTENT);
        }

        $contactService = $this->get('virtual_estates_api_contact');
        $contact = $contactService->getContactById($contactId);

        if (null == $contact) {
            return new Response($serializer->serialize($data, 'json'), Codes::HTTP_NOT_FOUND);
        }

        $service = $this->get('virtual_estates_api_property');

        try {
            $property = $service->createProperty($data, $contact);

            return new Response(
                $serializer->serialize($property, 'json'), 
                Codes::HTTP_CREATED,
                array('Location' =>  $this->generateUrl('get_property', array('propertyId' => $property->getPropertyId()), true))
            );
        } catch (ValidatorException $e) {
            return new Response(json_encode($e->getMessage()), Codes::HTTP_UNPROCESSABLE_ENTITY);
        } catch (Exception $e) {
            return new Response(json_encode($e->getMessage()), Codes::HTTP_BAD_REQUEST);
        }
    }
}
