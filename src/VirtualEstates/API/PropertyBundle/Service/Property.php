<?php

namespace VirtualEstates\API\PropertyBundle\Service;

use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\Validator\Exception\ValidatorException;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;

class Property {

    private $em;
    private $serializer;
    private $validator;

    public function __construct(
        EntityManager $em,
        RecursiveValidator $validator,
        Serializer $serializer
    )
    {
        $this->em = $em;
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    public function createProperty($data, $contact = null)
    {
        $property = $this->setProperty($data, $contact);
        $errors = $this->validator->validate($property);

        if (count($errors) == 0) {
            $this->em->persist($property);
            $this->em->flush();

            return $property;
        } else {
            throw new ValidatorException($errors);
        }
    }

    protected function setProperty($data, $contact = null)
    {
        $property = new \VirtualEstates\API\PropertyBundle\Entity\Property();

        foreach ($data as $field => $value) {
            $method = 'set'.ucfirst("$field");
            $property->$method($value);
        }

        $property->setCreated(new \Datetime());

        if ($contact != null) {
            $contact->addProperty($property);
            $property->setContact($contact);
        }

        return $property;
    }

    public function getSerializedProperties()
    {
        $properties = $this->em->getRepository('VirtualEstatesAPIPropertyBundle:Property')->findAll();

        return $this->serializer->serialize($properties, 'json');
    }

    public function getSerializedPropertiesByContact($contact)
    {
        return $this->serializer->serialize($contact->getProperties(), 'json');
    }

    public function deleteProperties()
    {
        $this->em->createQuery('DELETE FROM VirtualEstatesAPIPropertyBundle:Property')->execute();
    }
}