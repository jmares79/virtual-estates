<?php

namespace VirtualEstates\API\ContactBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Util\Codes;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ContactController extends Controller
{
    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns all contacts",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     204 = "Returned when no data received",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     *
     * @Route("/contacts",name="get_all_contacts")
     * @Method("GET")
     *
     * @return Response
     */
    public function getContactsAction()
    {
        $service = $this->get('virtual_estates_api_contact');
        
        return new Response($service->getSerializedContacts(), Response::HTTP_OK); 
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns a specific contact by id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     *
     * @Route("/contacts/{contactId}", name="get_contact", requirements={"contactId"="\d+"})
     * @Method("GET")
     *
     * @throws HttpException
     * @return Response
     */
    public function getContactAction($contactId)
    {
        $em = $this->getDoctrine()->getManager();
        $contact = $em->getRepository('VirtualEstatesAPIContactBundle:Contact')->findOneByContactId($contactId);

        if (null == $contact) {
            throw new HttpException(Codes::HTTP_NOT_FOUND, "The resource was not found");
        }

        $serializer = $this->get('jms_serializer');

        return new Response($serializer->serialize($contact, 'json'), Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a contact",
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     422 = "Returned when there was an error on creation"
     *   }
     * )
     *
     * @Route("/contacts", name="post_contact")
     * @Method("POST")
     *
     * @param  $request
     * @return response
     */
    public function postContactAction(Request $request)
    {
        $data = $request->request->all();
        $service = $this->get('virtual_estates_api_contact');

        if (!$service->hasMinimumData($data)) {
            return View::create($data, Codes::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $contact = $service->createContact($data);

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setStatusCode(Codes::HTTP_CREATED);

            $response->headers->set('Location',
                $this->generateUrl('get_contact', array('contactId' => $contact->getContactId()), true)
            );

            return $response;
        } catch (\Symfony\Component\Validator\Exception\ValidatorException $e) {
            return new Response($serializer->serialize($e->getMessage(), 'json'), Codes::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Updates a contact",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     201 = "Returned when created",
     *     204 = "Returned when no content received",
     *     400 = "Returned when there was an error on creation"
     *   }
     * )
     *
     * @Route("/contacts/{contactId}", name="put_contact", requirements={"contactId"="\d+"})
     * @Method("PUT")
     *
     * @param Request $request
     * @param $contactId
     *
     * @return Response
     */
    public function putContactAction(Request $request, $contactId)
    {
        $data = $request->request->all();
        $service = $this->get('virtual_estates_api_contact');

        if (null == $data) {
            return new Response($serializer->serialize($data, 'json'), Response::HTTP_NO_CONTENT);
        } 

        if (!$service->hasValidDataForUpdate($data)) {
            return new Response($serializer->serialize($data, 'json'), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $service->updateContact($contactId);

            return new Response($serializer->serialize($data, 'json'), Response::HTTP_OK);
        } catch (\Exception $e) {
            return new Response($serializer->serialize($e->getMessage(), 'json'), Codes::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Deletes a contact",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     201 = "Returned when created",
     *     204 = "Returned when no content received",
     *     400 = "Returned when there was an error on creation"
     *   }
     * )
     *
     * @Route("/contacts/{contactId}", name="delete_contact", requirements={"contactId"="\d+"})
     * @Method("DELETE")
     *
     * @param Request $request
     * @param $contactId
     * @internal request
     */
    public function deleteContactAction(Request $request, $contactId)
    {
        $data = $request->request->all();
        
        if (null == $data) {
            return new Response($serializer->serialize($data, 'json'), Codes::HTTP_NO_CONTENT);
        }
    }
}
