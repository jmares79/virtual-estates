<?php 

namespace VirtualEstates\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use VirtualEstates\UserBundle\Entity\User;
use VirtualEstates\API\ContactBundle\Entity\Contact;

class Contacts implements FixtureInterface
{
    public function getOrder()
    {
        return 20;
    }

    public function load(ObjectManager $manager)
    {
        $users = $manager->getRepository('UserBundle:User')->findAll();
        
        foreach ($users as $user) {
            for ($i = 0; $i < 15; $i++) {
                $contact = new Contact();

                $contact->setName($this->getName());
                $contact->setSurname($this->getSurname());
                $contact->setBirth(new \DateTime('now - '.rand(1000, 1500).' days'));
                $contact->setEmail(
                    strtolower(
                        $this->transformChars(str_replace(' ', '.', $contact->getName()))
                        .'.'.
                        $this->transformChars(str_replace(' ', '.', $contact->getSurname()))
                        .
                        $this->getEmailHost()
                    )
                );
                $contact->setSkype(
                    strtolower(
                        $this->transformChars(str_replace(' ', '.', $contact->getName()))
                        .'.'.
                        $this->transformChars(str_replace(' ', '.', $contact->getSurname()))
                        .
                        '@skype.com'
                    )
                );
                $contact->setUser($user);

                $manager->persist($contact);
            }
        }

        $manager->flush();
    }

    protected function getName()
    {
        $male = array(
            'Antonio', 'José', 'Manuel', 'Francisco', 'Juan', 'David',
            'José Antonio', 'José Luis', 'Jesús', 'Javier', 'Francisco Javier',
            'Carlos', 'Daniel', 'Miguel', 'Rafael', 'Pedro', 'José Manuel',
            'Ángel', 'Alejandro', 'Miguel Ángel', 'José María', 'Fernando',
            'Luis', 'Sergio', 'Pablo', 'Jorge', 'Alberto'
        );

        $female = array(
            'María Carmen', 'María', 'Carmen', 'Josefa', 'Isabel', 'Ana María',
            'María Dolores', 'María Pilar', 'María Teresa', 'Ana', 'Francisca',
            'Laura', 'Antonia', 'Dolores', 'María Angeles', 'Cristina', 'Marta',
            'María José', 'María Isabel', 'Pilar', 'María Luisa', 'Concepción',
            'Lucía', 'Mercedes', 'Manuela', 'Elena', 'Rosa María'
        );

        if (rand() % 2) {
            return $male[array_rand($male)];
        } else {
            return $female[array_rand($female)];
        }
    }

    protected function getSurname()
    {
        $lastnames = array(
            'García', 'González', 'Rodríguez', 'Fernández', 'López', 'Martínez',
            'Sánchez', 'Pérez', 'Gómez', 'Martín', 'Jiménez', 'Ruiz',
            'Hernández', 'Díaz', 'Moreno', 'Álvarez', 'Muñoz', 'Romero',
            'Alonso', 'Gutiérrez', 'Navarro', 'Torres', 'Domínguez', 'Vázquez',
            'Ramos', 'Gil', 'Ramírez', 'Serrano', 'Blanco', 'Suárez', 'Molina',
            'Morales', 'Ortega', 'Delgado', 'Castro', 'Ortíz', 'Rubio', 'Marín',
            'Sanz', 'Iglesias', 'Nuñez', 'Medina', 'Garrido'
        );

        return $lastnames[array_rand($lastnames)].' '.$lastnames[array_rand($lastnames)];
    }

    protected function getEmailHost()
    {
        $mail = array('@gmail.com', '@hotmail.com', '@yahoo.com');

        return $mail[array_rand($mail)];
    }

    protected function transformChars($string)
    {
        $to_replace = array('á', 'é', 'í', 'ó', 'ú', 'ñ', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ');
        $replacement = array('a', 'e', 'i', 'o', 'u', 'n', 'A', 'E', 'I', 'O', 'U', 'N');

        return str_replace($to_replace, $replacement, $string);
    }
}
