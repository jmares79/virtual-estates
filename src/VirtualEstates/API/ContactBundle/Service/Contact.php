<?php

namespace VirtualEstates\API\ContactBundle\Service;

use VirtualEstates\API\ContactBundle\Entity\Contact as ContactEntity;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\Validator\Exception\ValidatorException;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;

class Contact
{
    private $em;
    private $validator;
    private $serializer;
    const DATE_FORMAT = 'd-m-Y';

    public function __construct(
        EntityManager $em, 
        RecursiveValidator $validator, 
        Serializer $serializer
    )
    {
        $this->em = $em;
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    public function createContact($data)
    {
        $contact = $this->setContact($data);
        $errors = $this->validator->validate($contact);

        if (count($errors) == 0) {
            $this->em->persist($contact);
            $this->em->flush();
            
            return $contact;
        } else {
            throw new ValidatorException($errors);
        }
    }

    public function hasMinimumData($data)
    {
        if (null == $data) return false;
        if ( !isset($data['username']) || empty($data['username']) ) return false;

        return true;
    }

    protected function setContact($data, $user = null)
    {
        $contact = new ContactEntity();

        $contact->setName($data['name']);
        $contact->setSurname($data['surname']);
        $contact->setBirth(\DateTime::createFromFormat(self::DATE_FORMAT, str_replace('/', '-', $data['birth'])));
        $contact->setSkype($data['skype']);
        $contact->setEmail($data['email']);
        $contact->setCreated(new \Datetime());
        
        if (null != $user) $contact->setUser($user);

        return $contact;
    }

    protected function setAddresses($data)
    {

    }

    public function getSerializedContacts()
    {
        $contacts = $this->em->getRepository('VirtualEstatesAPIContactBundle:Contact')->findAll();

        return $this->serializer->serialize($contacts, 'json');
    }

    public function getContactById($contactId)
    {
        return $this->em->getRepository('VirtualEstatesAPIContactBundle:Contact')->findOneByContactId($contactId);
    }

    public function deleteContacts()
    {
        $this->em->createQuery('DELETE FROM VirtualEstatesAPIContactBundle:Contact')->execute();
    }
}