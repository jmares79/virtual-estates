<?php

namespace VirtualEstates\API\ContactBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use VirtualEstates\API\OperationBundle\Entity\Operation;
use VirtualEstates\API\PropertyBundle\Entity\Property;

/**
 * Contact
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VirtualEstates\API\ContactBundle\Entity\Repository\ContactRepository")
 * @UniqueEntity({"email"})
 */
class Contact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="contact_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $contactId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $surname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth", type="datetime")
     * @Assert\NotBlank()
     */
    private $birth;

    /**
     * @var string
     *
     * @ORM\Column(name="skype", type="string", length=255, nullable=true)
     */
    private $skype;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\ManyToMany(targetEntity="Address")
     * @ORM\JoinTable(name="contacts_addresses",
     *      joinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="contact_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="address_id", referencedColumnName="address_id")}
     * )
     */
    private $addresses;

    /**
     * @ORM\OneToMany(targetEntity="VirtualEstates\API\PropertyBundle\Entity\Property", mappedBy="contact")
     */
    private $properties;

    /**
     * @ORM\OneToMany(targetEntity="VirtualEstates\API\OperationBundle\Entity\Operation", mappedBy="contact")
     */
    private $operations;

    /**
     * @ORM\ManyToOne(targetEntity="VirtualEstates\UserBundle\Entity\User", inversedBy="contacts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

    public function __construct()
    {
        $this->addresses = new ArrayCollection();
        $this->operations = new ArrayCollection();
        $this->properties = new ArrayCollection();

        if ($this->getCreated() == null) {
            $this->setCreated(new \DateTime());
        }
    }

    public function __toString()
    {
        return $this->getName().' '.$this->getSurname();
    }

    /**
     * Get contactId
     *
     * @return integer
     */
    public function setContactId($contactId)
    {
        return $this->contactId = $contactId;
    }

    /**
     * Get contactId
     *
     * @return integer
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Contact
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Contact
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set birth
     *
     * @param \DateTime $birth
     *
     * @return Contact
     */
    public function setBirth($birth)
    {
        $this->birth = $birth;

        return $this;
    }

    /**
     * Get birth
     *
     * @return \DateTime
     */
    public function getBirth()
    {
        return $this->birth;
    }

    /**
     * Set skype
     *
     * @param string $skype
     *
     * @return Contact
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set emails
     *
     * @param string $email
     *
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set operations
     *
     * @param Operation $operations
     *
     * @return Contact
     */
    public function addOperation(Operation $operation)
    {
        $this->operations[] = $operation;

        return $this;
    }

    /**
     * Get operations
     *
     * @return string
     */
    public function getOperations()
    {
        return $this->operations;
    }

    /**
     * Set properties
     *
     * @param Property $property
     *
     * @return Contact
     */
    public function addProperty(Property $property)
    {
        $this->properties[] = $property;

        return $this;
    }

    /**
     * Get properties
     *
     * @return string
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * Has addresses
     *
     * @param Address $address
     *
     * @return boolean
     */
    public function hasAddress(Address $address)
    {
        foreach ($this->addresses as $loadedAddress) {
            if ($address == $loadedAddress) {
                return true;
            }
        }

        return false;
    }

    /**
     * Set a list of addresses
     *
     * @param array $addresses
     *
     * @return Contact
     */
    public function setAddresses($addresses)
    {
        foreach ($addresses as $address) {
            $this->addAddress($address);
        }

        return $this;
    }

    /**
     * Add an address
     *
     * @param Address $address
     *
     * @return Contact
     */
    public function addAddress(Address $address)
    {
        $this->addresses[] = $address;

        return $this;
    }

    /**
     * Get addresses
     *
     * @return ArrayCollection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Contact
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Contact
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set user
     *
     * @param \User $user
     *
     * @return Contact
     */
    public function setUser(\VirtualEstates\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Contact
     */
    public function getUser()
    {
        return $this->user;
    }
}

