<?php

namespace VirtualEstates\API\TaskBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Util\Codes;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\HttpException;
use VirtualEstates\API\TaskBundle\Entity\TaskType as TaskTypeEntity;

class TaskTypeController extends Controller
{
    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns all task types",
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Route("/tasktypes", name="get_all_task_types")
     * @Method("GET")
     *
     * @return Response
     */
    public function getTaskTypesAction()
    {
        $service = $this->get('virtual_estates_api_task');

        return new Response($service->getSerializedTaskTypes(), RESPONSE::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns a specific task type by id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     *
     * @Route("/tasktype/{taskTypeId}", name="get_task_type", requirements={"taskTypeId"="\d+"})
     * @Method("GET")
     * @ParamConverter("taskType", class="VirtualEstatesAPITaskBundle:TaskType")
     *
     * @throws HttpException
     * @return Response
     */
    public function getTaskTypeAction($taskType)
    {
        $serializer = $this->get('jms_serializer');

        return new Response($serializer->serialize($taskType, 'json'), Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a task type",
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     422 = "Returned when there was an error on creation"
     *   }
     * )
     *
     * @Route("/tasktype", name="post_task_type")
     * @Method("POST")
     *
     * @param $request
     * @return Response
     */
    public function postTaskTypeAction(Request $request)
    {
        $data = $request->request->all();

        $service = $this->get('virtual_estates_api_task');
        $serializer = $this->get('jms_serializer');

        if (!$service->hasMinimumTaskTypeData($data)) {
            return new Response($serializer->serialize($data, 'json'), Codes::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $taskType = $service->createTaskType($data);

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setStatusCode(Codes::HTTP_CREATED);

            $response->headers->set('Location',
                $this->generateUrl('get_task_type', array('taskTypeId' => $taskType->getTaskTypeId()), true)
            );

            return $response;
        } catch (\Symfony\Component\Validator\Exception\ValidatorException $e) {
            return new Response($serializer->serialize($e->getMessage(), 'json'), Codes::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Updates a task type",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     201 = "Returned when created",
     *     204 = "Returned when no content received",
     *     400 = "Returned when there was an error on creation"
     *   }
     * )
     *
     * @Route("/tasktype/{taskTypeId}", name="put_task_type", requirements={"taskTypeId"="\d+"})
     * @Method("PUT")
     * @ParamConverter("taskType", class="VirtualEstatesAPITaskBundle:TaskType")
     *
     * @param Request $request
     * @param $taskTypeId
     *
     * @return Response
     */
    public function putTaskTypeAction(Request $request, $taskType)
    {
        $data = $request->request->all();
        $service = $this->get('virtual_estates_api_task');

        if (null == $data) {
            return new Response($serializer->serialize($data, 'json'), Response::HTTP_NO_CONTENT);
        } 

        if (!$service->hasValidDataForUpdate($data)) {
            return new Response($serializer->serialize($data, 'json'), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $service->updateTaskType($taskTypeId);

            return new Response($serializer->serialize($data, 'json'), Response::HTTP_OK);
        } catch (\Exception $e) {
            return new Response($serializer->serialize($e->getMessage(), 'json'), Codes::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Deletes a task type",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     201 = "Returned when created",
     *     204 = "Returned when no content received",
     *     400 = "Returned when there was an error on creation"
     *   }
     * )
     *
     * @Route("/tasktype/{taskTypeId}", name="delete_task_type", requirements={"taskTypeId"="\d+"})
     * @Method("DELETE")
     * @ParamConverter("taskType", class="VirtualEstatesAPITaskBundle:TaskType")
     *
     * @param Request $request
     * @param VirtualEstates\API\TaskBundle\Entity\TaskType $taskType
     *
     * @return Response
     */
    public function deleteTaskTypeAction(Request $request, $taskType)
    {
        //TODO Implement DELETE
        return new Response($serializer->serialize($data, 'json'), Codes::HTTP_NOT_IMPLEMENTED);
        
        // $data = $request->request->all();
        
        // if (null == $data) {
        //     return new Response($serializer->serialize($data, 'json'), Codes::HTTP_NO_CONTENT);
        // }
    }
}
