<?php

namespace VirtualEstates\API\TaskBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Util\Codes;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\HttpException;

class TaskController extends Controller
{
    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns all tasks",
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Route("/tasks", name="get_all_tasks")
     * @Method("GET")
     *
     * @return Response
     */
    public function getTasksAction()
    {
        $service = $this->get('virtual_estates_api_task');

        return new Response($service->getSerializedTasks(), RESPONSE::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns a specific task by id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     *
     * @Route("/task/{taskId}", name="get_task", requirements={"taskId"="\d+"})
     * @Method("GET")
     *
     * @throws HttpException
     * @return Response
     */
    public function getTaskAction($taskId)
    {
        $em = $this->getDoctrine()->getManager();
        $task = $em->getRepository('VirtualEstatesAPITaskBundle:Task')->findOneByTaskId($taskId);

        if (null == $task) {
            throw new HttpException(Codes::HTTP_NOT_FOUND, "The resource was not found");
        }

        $serializer = $this->get('jms_serializer');

        return new Response($serializer->serialize($task, 'json'), Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a task",
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     422 = "Returned when there was an error on creation"
     *   }
     * )
     *
     * @Route("/task", name="post_task")
     * @Method("POST")
     *
     * @param $request
     * @return response
     */
    public function postTaskAction(Request $request)
    {
        $data = $request->request->all();
        $service = $this->get('virtual_estates_api_task');

        if (!$service->hasMinimumData($data)) {
            return View::create($data, Codes::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $task = $service->createTask($data);

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setStatusCode(Codes::HTTP_CREATED);

            $response->headers->set('Location',
                $this->generateUrl('get_task', array('taskId' => $task->getTaskId()), true)
            );

            return $response;
        } catch (\Symfony\Component\Validator\Exception\ValidatorException $e) {
            return new Response($serializer->serialize($e->getMessage(), 'json'), Codes::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Updates a task",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     201 = "Returned when created",
     *     204 = "Returned when no content received",
     *     400 = "Returned when there was an error on creation"
     *   }
     * )
     *
     * @Route("/tasks/{taskId}", name="put_task", requirements={"taskId"="\d+"})
     * @Method("PUT")
     *
     * @param Request $request
     * @param $taskId
     *
     * @return Response
     */
    public function putTaskAction(Request $request, $taskId)
    {
        $data = $request->request->all();
        $service = $this->get('virtual_estates_api_task');

        if (null == $data) {
            return new Response($serializer->serialize($data, 'json'), Response::HTTP_NO_CONTENT);
        } 

        if (!$service->hasValidDataForUpdate($data)) {
            return new Response($serializer->serialize($data, 'json'), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $service->updateTask($taskId);

            return new Response($serializer->serialize($data, 'json'), Response::HTTP_OK);
        } catch (\Exception $e) {
            return new Response($serializer->serialize($e->getMessage(), 'json'), Codes::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Deletes a task",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     201 = "Returned when created",
     *     204 = "Returned when no content received",
     *     400 = "Returned when there was an error on creation"
     *   }
     * )
     *
     * @Route("/task/{taskId}", name="delete_task", requirements={"taskId"="\d+"})
     * @Method("DELETE")
     *
     * @param Request $request
     * @param $taskId
     *
     * @return Response
     */
    public function deleteTaskAction(Request $request, $taskId)
    {
        $data = $request->request->all();

        if (null == $taskId) {
            return new Response($serializer->serialize($data, 'json'), Codes::HTTP_NO_CONTENT);
        }

        $service = $this->get('virtual_estates_api_task');
        $serializer = $this->get('jms_serializer');
        
        $task = $service->getTaskById($taskId);
        
        if (null == $task) {
            return new Response($serializer->serialize($data, 'json'), Codes::HTTP_NOT_FOUND);
        }

        try {
            $service->deleteTask($task);

            return new Response($serializer->serialize($data, 'json'), Response::HTTP_OK);
        } catch (\Exception $e) {
            return new Response($serializer->serialize($e->getMessage(), 'json'), Codes::HTTP_BAD_REQUEST);
        }
    }
}
