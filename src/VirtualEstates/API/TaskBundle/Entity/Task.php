<?php

namespace VirtualEstates\API\TaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Task
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Task
{
    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $taskId;

    /**
     * @ORM\ManyToOne(targetEntity="VirtualEstates\UserBundle\Entity\User", inversedBy="ownedTasks")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    private $userOwner;

    /**
     * @ORM\ManyToOne(targetEntity="VirtualEstates\UserBundle\Entity\User", inversedBy="assignedTasks")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    private $userAssigned;

    /**
     * @var VirtualEstates\API\TaskBundle\Entity\TaskType
     *
     * @ORM\ManyToOne(targetEntity="VirtualEstates\API\TaskBundle\Entity\TaskType")
     * @ORM\JoinColumn(name="task_type_id", referencedColumnName="task_type_id")
     */
    private $taskType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dueDate", type="datetime")
     * @Assert\NotBlank()
     */
    private $dueDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $status;


    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set userAssigned
     *
     * @param VirtualEstates\UserBundle\Entity\User $userAssigned
     *
     * @return Task
     */
    public function setUserAssigned($userAssigned)
    {
        $this->userAssigned = $userAssigned;

        return $this;
    }

    /**
     * Get userAssigned
     *
     * @return VirtualEstates\UserBundle\Entity\User
     */
    public function getUserAssigned()
    {
        return $this->userAssigned;
    }

    /**
     * Set userOwner
     *
     * @param VirtualEstates\UserBundle\Entity\User $userOwner
     *
     * @return Task
     */
    public function setUserOwner($userOwner)
    {
        $this->userOwner = $userOwner;

        return $this;
    }

    /**
     * Get userOwner
     *
     * @return VirtualEstates\UserBundle\Entity\User
     */
    public function getUserOwner()
    {
        return $this->userOwner;
    }

    /**
     * Set taskType
     *
     * @param VirtualEstates\API\TaskBundle\Entity\TaskType $taskType
     *
     * @return Task
     */
    public function setTaskType($taskType)
    {
        $this->taskType = $taskType;

        return $this;
    }

    /**
     * Get taskType
     *
     * @return VirtualEstates\API\TaskBundle\Entity\TaskType
     */
    public function getTaskType()
    {
        return $this->taskType;
    }

    /**
     * Set due date
     *
     * @param \DateTime $dueDate
     *
     * @return Task
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get due date
     *
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Task
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param string $updated
     *
     * @return Task
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return string
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Task
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
