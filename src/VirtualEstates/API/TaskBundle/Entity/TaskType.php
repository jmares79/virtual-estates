<?php

namespace VirtualEstates\API\TaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * TaskType
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class TaskType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="task_type_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $taskTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * Get id
     *
     * @return integer
     */
    public function getTaskTypeId()
    {
        return $this->taskTypeId;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return TaskType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
