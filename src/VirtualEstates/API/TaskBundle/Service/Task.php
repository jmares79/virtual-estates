<?php

namespace VirtualEstates\API\TaskBundle\Service;

use VirtualEstates\API\TaskBundle\Entity\Task as TaskEntity;
use VirtualEstates\API\TaskBundle\Entity\TaskType as TaskTypeEntity;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\Validator\Exception\ValidatorException;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;

class Task
{
    private $em;
    private $validator;
    private $serializer;
    const DATE_FORMAT = 'd-m-Y';

    private $taskType;
    private $userOwner;
    private $userAssigned;

    public function __construct(
        EntityManager $em,
        RecursiveValidator $validator,
        Serializer $serializer
    )
    {
        $this->em = $em;
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    public function createTask($data)
    {
        $task = $this->setTask($data);
        $errors = $this->validator->validate($task);

        if (count($errors) == 0) {
            $this->em->persist($task);
            $this->em->flush();
            
            return $task;
        } else {
            throw new ValidatorException($errors);
        }
    }

    public function hasMinimumData($data)
    {
        if (null == $data) return false;

        if ( !isset($data['userOwnerId']) || empty($data['userOwnerId']) ) return false;
        if ( !isset($data['userAssignedId']) || empty($data['userAssignedId']) ) return false;
        if ( !isset($data['taskTypeId']) || empty($data['taskTypeId']) ) return false;

        $this->taskType = $this->getTaskTypeById($data['taskTypeId']);
        $this->userOwner = $this->em->getRepository('UserBundle:User')->findOneByUserId($data['userOwnerId']);
        $this->userAssigned = $this->em->getRepository('UserBundle:User')->findOneByUserId($data['userAssignedId']);

        if ($this->taskType == null || $this->userOwner == null || $this->userAssigned == null) return false;

        return true;
    }

    protected function setTask($data)
    {
        $task = new TaskEntity();

        $task->setUserAssigned($this->userAssigned);
        $task->setUserOwner($this->userOwner);
        $task->setTaskType($this->taskType);
        $task->setDueDate(\DateTime::createFromFormat(self::DATE_FORMAT, str_replace('/', '-', $data['dueDate'])));

        if (!isset($data['created']) || empty($data['created'])) {
            $task->setCreated(new \DateTime());
        } else {
            $task->setCreated(\DateTime::createFromFormat(self::DATE_FORMAT, str_replace('/', '-', $created)));
        }

        $task->setStatus($data['status']);
        
        return $task;
    }


    public function getSerializedTasks()
    {
        $tasks = $this->em->getRepository('VirtualEstatesAPITaskBundle:Task')->findAll();

        return $this->serializer->serialize($tasks, 'json');
    }

    public function getTaskById($taskId)
    {
        return $this->em->getRepository('VirtualEstatesAPITaskBundle:Task')->findOneByTaskId($taskId);
    }

    public function getTaskTypeById($taskTypeId)
    {
        return $this->em->getRepository('VirtualEstatesAPITaskBundle:TaskType')->findOneByTaskTypeId($taskTypeId);
    }

    public function deleteTask($task)
    {
        $this->em->remove($task);
        $this->em->flush();
    }

    public function createTaskType($data)
    {
        $taskType = $this->setTaskType($data);
        $errors = $this->validator->validate($taskType);

        if (count($errors) == 0) {
            $this->em->persist($taskType);
            $this->em->flush();
            
            return $taskType;
        } else {
            throw new ValidatorException($errors);
        }
    }

    public function hasMinimumTaskTypeData($data)
    {
        if (null == $data) return false;
        if ( !isset($data['description']) || empty($data['description']) ) return false;

        return true;
    }

    protected function setTaskType($data)
    {
        $taskType = new TaskTypeEntity();

        $taskType->setDescription(ucfirst($data['description']));

        return $taskType;
    }
}
