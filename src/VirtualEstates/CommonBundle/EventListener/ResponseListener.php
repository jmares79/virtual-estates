<?php

namespace VirtualEstates\CommonBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class ResponseListener
{
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $request = $event->getRequest();

        if ($request->getRequestFormat() != 'json') {
            return;
        }

        $event->getResponse()->headers->set('Content-Type', 'application/json');
        $event->getResponse()->headers->set('Access-Control-Allow-Origin', '*');
        $event->getResponse()->headers->set('Access-Control-Allow-Methods', 'GET, POST, PUT');
    }
}