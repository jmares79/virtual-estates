Virtual Estates
========================

# Introduction

Virtual Estates project was created as an API to feed a Front End (not included) for creating pages for small to medium size real estates agencies.

What is the main issue with those agencies when they have to create a web page? The issue is the creation/maintenance costs.
There are 2 ways of having a web site, either you buy one (with the code and data) and maintain yourself, or you rent one.
The first option is rather expensive for small busines, as they have, after having the code, deploy and maintain it, which is not an option.

This API is the first step in provide an on the fly creation of a real state site.

What's inside?
--------------

The core of the software is done in PHP, using Symfony Standard Edition, with some add ons explained below.
Symfony structure is oganizaed in Bundles, which are:

  * ContactBundle: self explanatory

  * OperationBundle: It handles all type of operations between some potential client and an agent, as rent, sale, buy, etc

  * PropertyBundle self explanatory

  * TaskBundle (Not finished)

  * MailBundle (Not finished)

  * UserBundle: handles the registering and authentication of users

## Installation

For using this soft you'll nedd PHP, any web server, Git for versioning and [**composer**][1] for managing packages.
Having installed those, the steps are:

1) Clone the repo
2) Within the desired folder, execute: `composer install`
3) Run your web server
4) Check routes within controllers to see what's going on

Being a Symfony project, it comes pre-configured with the following bundles of course:

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * [**AsseticBundle**][12] - Adds support for Assetic, an asset processing
    library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][13] (in dev/test env) - Adds code generation
    capabilities

  * **DebugBundle** (in dev/test env) - Adds Debug and VarDumper component
    integration

## Tests

Some test were provided for the project. [**Behat**][14] is a framework for a [**BDD**][15] approach.

Basically you create a 'action.feature' text file with a header explaining the purpose of the test and later a
'Scenario' with data and the steps that the tests will execute.

At the same time, you have to create a PHP class extending and/or implementing some classes/interfaces with methods
that have an annotation equal to the steps written in the 'Scenario' before.

For the details refer to the documentation.

## Documentation

The documentation is created using Nelmio API doc bundle. All classes and methods that worth documenting has annotations
explaining its purpose, arguments, returning values and exceptions, if any.


[1]:  https://getcomposer.org/
[7]:  https://symfony.com/doc/2.7/book/doctrine.html
[9]:  https://symfony.com/doc/2.7/book/security.html
[10]: https://symfony.com/doc/2.7/cookbook/email.html
[11]: https://symfony.com/doc/2.7/cookbook/logging/monolog.html
[12]: https://symfony.com/doc/2.7/cookbook/assetic/asset_management.html
[13]: https://symfony.com/doc/2.7/bundles/SensioGeneratorBundle/index.html
[14]: http://behat.org/en/latest/
[15]: https://en.wikipedia.org/wiki/Behavior-driven_development
