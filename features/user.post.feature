 Feature: User
  In order to use projects
  As an API client
  I need to be able to create users
  
  Scenario: Create a user
    Given I have the payload:
      """
      {
        "name": "Pedro",
        "lastname" : "Rodriguez",
        "username": "pedro.rodriguez",
        "email": "pedro.rod@gmail.com",
        "password": "password",
        "birth": "10/10/1970",
        "type": "normal"
      }
      """
    When I request "POST" "/users"
    Then the response status code should be "201"
    And the "Location" header should be "http://estates.localhost.dev/users/pedro.rodriguez"