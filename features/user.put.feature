 Feature: User
  In order to use projects
  As an API client
  I need to be able to update users

  Scenario: Valid update of an existing user modifying it
    Given the following user exist:
      | name    | lastname  | username        | email               |  password  | birth      | type   |
      | Pedro   | Rodriguez | pedro.rodriguez | pedro.rod@gmail.com |  password  | 10/10/1970 | normal |
    And I have the payload:
      """
      {
        "name": "Pedro",
        "lastname" : "Oriolo",
        "username": "pedro.rodriguez",
        "type": "silver"
      }
      """
    When I request "PUT" "/users"
    Then the response status code should be "200"

  Scenario: Valid update of an existing user without modify it
    Given the following user exist:
      | name    | lastname  | username        | email               |  password  | birth      | type   |
      | Pedro   | Rodriguez | pedro.rodriguez | pedro.rod@gmail.com |  password  | 10/10/1970 | normal |
    And I have the payload:
      """
      {
        "name": "Pedro",
        "lastname" : "Rodriguez",
        "username": "pedro.rodriguez",
        "type": "normal"
      }
      """
    When I request "PUT" "/users"
    Then the response status code should be "200"

  Scenario: Valid update of the password
    Given the following user exist:
      | name    | lastname  | username        | email               |  password  | birth      | type   |
      | Pedro   | Rodriguez | pedro.rodriguez | pedro.rod@gmail.com |  password  | 10/10/1970 | normal |
    And I have the payload:
      """
      {
        "username": "pedro.rodriguez",
        "password": "new_password"
      }
      """
    When I request "PUT" "/users"
    Then the response status code should be "200"

  Scenario: Valid update of the birth
    Given the following user exist:
      | name    | lastname  | username        | email               |  password  | birth      | type   |
      | Pedro   | Rodriguez | pedro.rodriguez | pedro.rod@gmail.com |  password  | 10/10/1970 | normal |
    And I have the payload:
      """
      {
        "username": "pedro.rodriguez",
        "birth": "10-02-1990"
      }
      """
    When I request "PUT" "/users"
    Then the response status code should be "200"

  Scenario: Invalid update of the username
    Given the following user exist:
      | name    | lastname  | username        | email               |  password  | birth      | type   |
      | Pedro   | Rodriguez | pedro.rodriguez | pedro.rod@gmail.com |  password  | 10/10/1970 | normal |
    And I have the payload:
      """
      {
        "name": "Pedro",
        "lastname" : "Rodriguez",
        "username": "pedro.gutierrez",
        "type": "normal"
      }
      """
    When I request "PUT" "/users"
    Then the response status code should be "404"

  Scenario: Invalid data for updating, username missed
    Given the following user exist:
      | name    | lastname  | username        | email               |  password  | birth      | type   |
      | Pedro   | Rodriguez | pedro.rodriguez | pedro.rod@gmail.com |  password  | 10/10/1970 | normal |
    And I have the payload:
      """
      {
        "email": "pedro.rod@hotmail.com"
      }
      """
    When I request "PUT" "/users"
    Then the response status code should be "400"

  Scenario: Invalid update of the birth
    Given the following user exist:
      | name    | lastname  | username        | email               |  password  | birth      | type   |
      | Pedro   | Rodriguez | pedro.rodriguez | pedro.rod@gmail.com |  password  | 10/10/1970 | normal |
    And I have the payload:
      """
      {
        "birth": "10/15/1970"
      }
      """
    When I request "PUT" "/users"
    Then the response status code should be "400"

  Scenario: Invalid update of the user
     Given the following user exist:
      | name    | lastname  | username        | email               |  password  | birth      | type   |
      | Pedro   | Rodriguez | pedro.rodriguez | pedro.rod@gmail.com |  password  | 10/10/1970 | normal |
      
    And I have the payload:
      """
      {
        "username": "juan.perez",
        "birth": "10/15/1970"
      }
      """
    When I request "PUT" "/users"
    Then the response status code should be "404"
