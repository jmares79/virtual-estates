Feature: User
  In order to use projects
  As an API client
  I need to be able to get users

  Scenario: Get a collection of users
    Given the following user exist:
      | name    | lastname  | username        | email               |  password  | birth      | type   |
      | Pedro   | Rodriguez | pedro.rodriguez | pedro.rod@gmail.com |  password  | 10/10/1970 | normal |
    When I request "GET" "/users"
    Then the response status code should be "200"
    And the array should contain 1 item

 