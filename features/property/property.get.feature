Feature: Property
  In order to use projects
  As an API client
  I need to be able to get properties

  Scenario: Get a collection of properties
    Given the following property exist:
      |  address                        |bathrooms|parkings|bedrooms|totalSurface|coveredSurface|uncoveredSurface|fieldSurface|fieldWidth|fieldGround|constructionYear|floor|totalFloors|monthlyMaintenance|
      |  6 Woodside, SW197AR, Wimbledon | 2       |        | 3      |  89.5      |              |                |            |          |           |                |  3  | 10        | 450.50           |
    When I request "GET" "/properties"
    Then the response status code should be "200"
    And the array should contain 1 item