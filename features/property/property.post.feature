Feature: Property
  In order to use projects
  As an API client
  I need to be able to create properties

  Scenario: Create a property
    Given I have the payload:
      """
      {
        "address": "6 Woodside, SW197AR, Wimbledon",
        "bathrooms" : "2",
        "parkings": "",
        "bedrooms": "3",
        "totalSurface": "89.5",
        "coveredSurface": "",
        "uncoveredSurface": "",
        "fieldSurface": "",
        "fieldWidth": "",
        "fieldGround": "",
        "constructionYear": "",
        "floor": "3",
        "totalFloors": "10",
        "monthlyMaintenance": "450.50"
      }
      """
    And the following contacts exist:
    | contactId | name  | surname | birth     | email               | facebook           | skype | gender |
    |  15       | John  | Wood    | 1/10/1945 | john.wood@gmail.com | john.wood@facebook |       | male   |
    When I request "POST" "/property/contact/15"
    Then the response status code should be "201"
    And the "Location" header should have the format "\/property\/[0-9]+"