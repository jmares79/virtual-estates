Feature: Contact
  In order to use projects
  As an API client
  I need to be able to get contacts

  Scenario: Get a collection of contacts
    Given the following contacts exist:
      | name   | surname   | birth     | email                |  facebook           | skype | gender |
      | John   | Wood      | 1/10/1945 | john.wood@gmail.com  |  john.wood@facebook |       | male   |
      | Paul   | McCartney | 2/3/1940  | paul.macca@gmail.com |                     |       | male   |
    When I request "GET" "/contacts"
    Then the response status code should be "200"
    And the array should contain 2 item