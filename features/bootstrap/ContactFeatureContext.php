<?php

use Behat\Behat\Context\ClosuredContextInterface;
use Behat\Behat\Context\TranslatedContextInterface;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\MinkExtension\Context\MinkContext;

use Guzzle\Http\Client;
use Guzzle\Http\Message\Request;
use Guzzle\Http\Message\Response;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\BadResponseException;

require_once __DIR__.'/../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';

class ContactFeatureContext extends MinkContext implements SnippetAcceptingContext
{
    private $client;
    private $response;
    private $payload;
    private $service;

    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     * @param VirtualEstates\API\ContactBundle\Service\Contact $service
     */
    public function __construct(VirtualEstates\API\ContactBundle\Service\Contact $service)
    {
        $this->service = $service;
        $this->payload = new stdClass();
       
        $this->client = new GuzzleHttp\Client([
            'base_uri' => 'http://estates.localhost.dev/app_dev.php',
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept'       => '*/*'
            ]
        ]); 
    }

    /**
     * @BeforeScenario
     */
    public function clearData()
    {
        $this->service->deleteContacts();
    }

    /**
     * @Given the following contacts exist:
     *
     * @param TableNode $table
     */
    public function theFollowingContactsExist(TableNode $table)
    {
        throw new PendingException();
    }

    /**
     * @When I request :arg1 :arg2
     */
    public function iRequest($arg1, $arg2)
    {
        throw new PendingException();
    }

    /**
     * @Then the response status code should be :arg1
     */
    public function theResponseStatusCodeShouldBe($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Then the array should contain :arg1 item
     */
    public function theArrayShouldContainItem($arg1)
    {
        throw new PendingException();
    }

}