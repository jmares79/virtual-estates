<?php

use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Gherkin\Node\PyStringNode;

use GuzzleHttp\Exception\ClientException;

require_once __DIR__.'/../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';

class PropertyFeatureContext extends MinkContext implements SnippetAcceptingContext
{
    private $client;
    private $response;
    private $payload;
    private $service;
    private $contactService;

    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     * @param VirtualEstates\API\PropertyBundle\Service\Property $service
     * @param VirtualEstates\API\ContactBundle\Service\Contact $contactService
     */
    public function __construct(
        VirtualEstates\API\PropertyBundle\Service\Property $service,
        VirtualEstates\API\ContactBundle\Service\Contact $contactService
    )
    {
        $this->service = $service;
        $this->contactService = $contactService;
        $this->payload = new stdClass();

        $this->client = new GuzzleHttp\Client([
            'base_uri' => 'http://estates.localhost.dev/app_dev.php',
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept'       => '*/*'
            ]
        ]);
    }

    /**
     * @BeforeScenario
     */
    public function clearData()
    {
        $this->service->deleteProperties();
        $this->contactService->deleteContacts();
    }

    /**
     * @Given the following property exist:
     *
     * @param TableNode $table The table with the data to be loaded
     */
    public function theFollowingPropertyExist(TableNode $table)
    {
        foreach ($table->getHash() as $data) {
            $this->service->createProperty($data);
        }
    }

    /**
     * @When I request :arg1 :arg2
     *
     * @param string $httpMethod
     * @param string $url
     */
    public function iRequest($httpMethod, $url)
    {
        try {
            switch(strtoupper($httpMethod)) {
                case 'POST':
                    $this->response = $this->client->request('POST', $url,
                        [
                            'json' => [
                                "address" => $this->payload->address,
                                "bathrooms" => $this->payload->bathrooms,
                                "parkings" => $this->payload->parkings,
                                "bedrooms" => $this->payload->bedrooms,
                                "totalSurface" => $this->payload->totalSurface,
                                "coveredSurface" => $this->payload->coveredSurface,
                                "uncoveredSurface" => $this->payload->uncoveredSurface,
                                "fieldSurface" => $this->payload->fieldSurface,
                                "fieldWidth" => $this->payload->fieldWidth,
                                "fieldGround" => $this->payload->fieldGround,
                                "constructionYear" => $this->payload->constructionYear,
                                "floor" => $this->payload->floor,
                                "totalFloors" => $this->payload->totalFloors,
                                "monthlyMaintenance" => $this->payload->monthlyMaintenance
                            ]
                        ]
                    );
                    break;
                case 'PUT':
                    $this->response = $this->client->request('PUT', $url,
                        [
                            'json' => [
                                "address" => $this->getField("address"),
                                "bathrooms" => $this->getField("bathrooms"),
                                "parkings" => $this->getField("parkings"),
                                "bedrooms" => $this->getField("bedrooms"),
                                "totalSurface" => $this->getField("totalSurface"),
                                "coveredSurface" => $this->getField("coveredSurface"),
                                "uncoveredSurface" => $this->getField("uncoveredSurface"),
                                "fieldSurface" => $this->getField("fieldSurface"),
                                "fieldWidth" => $this->getField("fieldWidth"),
                                "fieldGround" => $this->getField("fieldGround"),
                                "constructionYear" => $this->getField("constructionYear"),
                                "floor" => $this->getField("floor"),
                                "totalFloors" => $this->getField("totalFloors"),
                                "monthlyMaintenance" => $this->getField("monthlyMaintenance")
                            ]
                        ]
                    );
                    break;

                case 'GET':
                    $this->response = $this->client->request('GET', $url);
                    break;
            }
        } catch (ClientException $e) {
            $this->response = $e->getResponse();
        }
    }

    protected function getField($field)
    {
        return isset($this->payload->{$field}) ? $this->payload->{$field} : '';
    }
    /**
     * @Then the response status code should be :arg1
     *
     * @param string $status
     */
    public function theResponseStatusCodeShouldBe($status)
    {
        $returnedStatus = $this->response->getStatusCode();
        $message = sprintf("Status code should be %s but is %s instead", $status, $returnedStatus);

        assertEquals($status, $returnedStatus, $message);
    }

    /**
     * @Given /^the array should contain (\d+) item$/
     *
     * @param int $count
     */
    public function theArrayShouldContainItem($count)
    {
        $items = count(json_decode($this->response->getBody(true)));
        $message = sprintf("Array should contain %d items but contains %d instead", $count, $items);

        assertEquals($count, $items, $message);
    }

    /**
     * @Given /^I have the payload:$/
     *
     * @param PyStringNode $payload The string with the payload data to be used
     */
    public function iHaveThePayload(PyStringNode $payload)
    {
        $this->payload = json_decode($payload);
    }

    /**
     * @Given the following contacts exist:
     *
     * @param TableNode $table
     */
    public function theFollowingContactsExist(TableNode $table)
    {
        foreach ($table->getHash() as $data) {
            $this->contactService->createContact($data);
        }
    }

    /**
     * @Then the :arg1 header should have the format :arg2
     *
     * @param string $headerName The name of the header to be inspected
     * @param string $urlPattern URL with a placeholder to be inspected
     */
    public function theHeaderShouldHaveTheFormat($headerName, $urlPattern)
    {
        $receivedUrl = $this->response->getHeader($headerName)[0];
        $message = sprintf("The URL should have the format %s but has %s format", $urlPattern, $receivedUrl);

        assertRegExp($urlPattern, $receivedUrl, $message);
    }
}