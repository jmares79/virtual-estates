<?php

use Behat\Behat\Context\ClosuredContextInterface;
use Behat\Behat\Context\TranslatedContextInterface;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\MinkExtension\Context\MinkContext;

use Guzzle\Http\Client;
use Guzzle\Http\Message\Request;
use Guzzle\Http\Message\Response;
use GuzzleHttp\Exception\ClientException;

require_once __DIR__.'/../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';

class FeatureContext extends MinkContext implements SnippetAcceptingContext
{
    private $_client;
    private $_response;
    private $_payload;
    private $_service;

    /**
     * Initializes context.
     * Every scenario gets its own context object.
     */
    public function __construct(VirtualEstates\UserBundle\Service\User $service)
    {
        $this->_service = $service;
        $this->_payload = new stdClass();
       
        $this->_client = new GuzzleHttp\Client([
            'base_uri' => 'http://estates.localhost.dev/app_dev.php',
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept'       => '*/*'
            ]
        ]); 
    }

    /**
     * @BeforeScenario
     */
    public function clearData()
    {
       $this->_service->deleteUsers();
    }

    /**
     * @Given /^the following user exist:$/
     *
     * @param TableNode $table The table with the data to be loaded
     */
    public function theFollowingUserExist(TableNode $table)
    {
        foreach ($table->getHash() as $data) {
            $this->_service->createUser($data);
        }
    }

    /**
     * @Given /^I have the payload:$/
     *
     * @param PyStringNode $payload The string with the payload data to be used
     */
    public function iHaveThePayload(PyStringNode $payload)
    {
        $this->_payload = json_decode($payload);
    }

    /**
     * @When /^I request "(GET|PUT|POST|DELETE)" "([^"]*)"$/
     *
     * @param string $httpMethod
     * @param string $url
     */
    public function iRequest($httpMethod, $url)
    {
        try {
            switch(strtoupper($httpMethod)) {
                case 'POST':
                    $this->_response = $this->_client->request('POST', $url,
                        [
                            'json' => [
                                "name" => $this->_payload->name,
                                "lastname" => $this->_payload->lastname,
                                "username" => $this->_payload->username,
                                "email" => $this->_payload->email,
                                "password" => $this->_payload->password,
                                "birth" => $this->_payload->birth,
                                "type" => $this->_payload->type
                            ]
                        ]
                    );
                break;
                case 'PUT':
                    $this->_response = $this->_client->request('PUT', $url,
                        [
                            'json' => [
                                "name" => isset($this->_payload->name) ? $this->_payload->name : '',
                                "lastname" => isset($this->_payload->lastname) ? $this->_payload->lastname : '' ,
                                "username" => isset($this->_payload->username) ? $this->_payload->username : '',
                                "email" => isset($this->_payload->email) ? $this->_payload->email : '',
                                "password" => isset($this->_payload->password) ? $this->_payload->password : '',
                                "birth" => isset($this->_payload->birth) ? $this->_payload->birth : '',
                                "type" => isset($this->_payload->type) ? $this->_payload->type : ''
                            ]
                        ]
                    );
                break;

                case 'GET':
                    $this->_response = $this->_client->request('GET', $url);
                break;
            }
        } catch (ClientException $e) {
            $this->_response = $e->getResponse();
        }
    }

    /**
     * @Given /^the "([^"]*)" header should be "([^"]*)"$/
     *
     * @param $headerName
     * @param $expected
     */
    public function theHeaderShouldBe($headerName, $expected)
    {
        $receivedHeader = $this->_response->getHeader($headerName)[0];

        assertEquals($expected, $receivedHeader, sprintf("Value should be %s but is %s instead", $expected, $receivedHeader));
    }

    /**
     * @Given /^the array should contain (\d+) item$/
     *
     * @param int $count
     */
    public function theArrayShouldContainItem($count)
    {
        $items = count(json_decode($this->_response->getBody(true)));
        $message = sprintf("Array should contain %d items but contains %d instead", $count, $items);

        assertEquals($count, $items, $message);
    }

    /**
     * @Then the response status code should be :arg1
     *
     * @param string $status
     */
    public function theResponseStatusCodeShouldBe($status)
    {
        $returnedStatus = $this->_response->getStatusCode();
        $message = sprintf("Status code should be %s but is %s instead", $status, $returnedStatus);

        assertEquals($status, $returnedStatus, $message);
    }

    /**
     * @Given there is no users in the database:
     */
    public function thereIsNoUsersInTheDatabase()
    {
        $this->clearData();
    }
}
